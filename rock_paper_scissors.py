from numpy import random
import numpy as np
import os

def title():
    print("***** Cùng chơi kéo, búa, bao với Tao nào *****")

def menu():
    print("Chọn kéo, búa, bao bằng cách nhập một số theo menu dưới đây:")
    print("* Kéo : 1")
    print("* Búa : 2")
    print("* Bao : 3")

def resultRound(roundGame, chooseMe, chooseUser, check, userWin, meWin):
    resultStr = ""
    print("Thông báo kết quả lượt " + str(roundGame) + ":")
    print("Tao đã chọn " + checkChosse(chooseMe))
    print("Mày đã chọn " + checkChosse(chooseUser))

    if check == 1:
        resultStr = "Mày đã thắng"
    elif check == 2:
        resultStr = "Mày đã thua"
    else:
        resultStr = "Hòa"
    
    print(resultStr + " và tỉ số là " + str(userWin) + " - " + str(meWin))

def space():
    print("--------------------------------------------")

def checkChosse(choose):
    chooseStr = ""

    if choose == 1:
        chooseStr = "Kéo"
    elif choose == 2:
        chooseStr = "Búa"
    else:
        chooseStr = "Bao"

    return chooseStr

def inputRound(roundGame):
    return int(input("Nhập lựa chọn của mày trong lượt " + str(roundGame) + ": "))

def startGame():
    userWinArr = np.array([[1, 3], [2, 1], [3,2]])
    roundGame = 1
    userWin = 0
    meWin = 0
    check = 0
    while roundGame <= 3:
        chooseUser = 0
        chooseMe = random.randint(1, 3)
        while chooseUser not in [1, 2, 3]:
            try:
                chooseUser = inputRound(roundGame)
            except:
                continue
        
        realArr = np.array([chooseUser, chooseMe])
        
        if chooseMe == chooseUser:
            check = 0
        elif any(np.array_equal(x, realArr) for x in userWinArr):
            userWin = userWin + 1
            check = 1
        else:
            meWin = meWin + 1
            check = 2

        resultRound(roundGame, chooseMe, chooseUser, check, userWin,meWin)

        if userWin == 2 or meWin == 2:
            space()
            break

        roundGame = roundGame + 1

        space()

    return [meWin, userWin]

def summary(result):
    if result[0] > result[1]:
        print("Bạn thua mẹ rồi! Hihi")
    elif result[0] < result[1]:
        print("Mày thắng?? Thì ra mày chọn cái chết!")
    else:
        print("Éo hiểu sao hòa được!! Vô lý")

def main():
    title()
    space()
    menu()
    space()
    result = startGame()
    summary(result)
    os.system("pause")

if __name__ == '__main__':
    main()
